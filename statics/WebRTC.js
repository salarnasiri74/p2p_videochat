const url = 'https://' + document.domain + ':' + location.port + "/chat";
const uuid = Uuid();
var socket;
var room = 'default';

var localVideo;
var remoteVideo;
var peerConnection;
var isCaller;

var peerConnectionConfig = {
    'iceServers': [
        {'urls': 'stun:stun.services.mozilla.com'},
        {'urls': 'stun:stun.l.google.com:19302'},
        {
            'urls': 'turn:turn.salar.click:3478?transport=udp',
            'credential': 'KoalaTeam',
            'username': 'salar'
        }
    ]
};

var constraints = {
    video:true,
    audio:true
};

function Uuid() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}


function sendBySocket(perfix, data){
	data.uuid = uuid;
	data.room = room;
	socket.emit(perfix, data);
}

function sendICE(ICE){
	sendBySocket('ice', {'ice': ICE});
}

function sendSDP(SDP){
	sendBySocket('sdp', {'sdp': SDP});
}


function startChat() {
	localVideo = document.getElementById('localVideo');
	remoteVideo = document.getElementById('remoteVideo');

	peerConnection = new RTCPeerConnection(peerConnectionConfig);
//	peerConnection.onaddstream = gotRemoteStream;
	peerConnection.onicecandidate = gotIceCandidate;    
	peerConnection.onaddstream = gotRemoteStream;

	if(navigator.mediaDevices.getUserMedia) {
		navigator.mediaDevices.getUserMedia(constraints).then(getUserMediaSuccess).catch(errorHandler);
	}
	else {
		alert('Your browser does not support getUserMedia API');
	}
}

function getUserMediaSuccess(stream) {
	console.log("getUserMediaSuccess");
	localStream = stream;
	localVideo.src = window.URL.createObjectURL(stream);
	peerConnection.addStream(localStream);
	

	if(isCaller){
		peerConnection.createOffer().then(createdDescription).catch(errorHandler);
	}
}


function gotIceCandidate(event) {
	if(event.candidate != null)
		sendICE(event.candidate);
}


function gotRemoteStream(event) {
	console.log('gotRemoteStreamSuccess');
	remoteVideo.src = window.URL.createObjectURL(event.stream);
}


function createdDescription(description) {
	peerConnection.setLocalDescription(description).then(function() {
		sendSDP(peerConnection.localDescription);
	}).catch(errorHandler);
}


function errorHandler(error) {
	console.log(error);
}
