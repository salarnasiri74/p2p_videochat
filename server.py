from Queue import Queue
from flask import Flask, request, render_template, Response, redirect, url_for

from flask_socketio import SocketIO, send, emit, join_room, leave_room
import time

app = Flask(__name__, template_folder='.', static_folder='statics')
user_ids = {}


@app.route('/')
def index():
    return redirect(url_for('phase1'))


@app.route("/phase1")
def phase1():
    return render_template('phase1.html')


@app.route("/turn")
def turn_checker():
    return render_template('turn.html')


@app.route('/phase2')
def phase2():
    return render_template('phase2.html')


@app.route('/xhr/<string:uid>', methods=['POST'])
def handle_xhr(uid):
    global user_ids

    if not uid in user_ids:
        return 'user does not exists'
    user_ids[uid].put(request.data)
    return 'sent'


@app.route('/sse/<string:uid>', methods=['GET'])
def handle_sse(uid):
    global user_ids

    if uid in user_ids:
        user_ids[uid].put(None)
        del user_ids[uid]
    user_ids[uid] = Queue()

    def events():
        while True:
            data = user_ids[uid].get()
            if not data:
                return
            yield "data: %s\n\n" % data

    return Response(events(), content_type='text/event-stream')


socketio = SocketIO(app)

rooms = {}


@socketio.on('connect', namespace='/chat')
def test_connect():
    print('connected!')
    emit('my response', {'data': 'Connected'})


@socketio.on('join', namespace='/chat')
def on_join(data):
    uuid = data['uuid']
    room = data['room']
    if room not in rooms:
        rooms[room] = []
    rooms[room].append(
        {
            'uuid': uuid,
            'status': False
        }
    )
    print(str(uuid) + ' joined! ' + room)
    join_room(room)


@socketio.on('sdp', '/chat')
def get_message(data):
    uuid = data['uuid']
    room = data['room']
    emit('sdp', data, room=room)


@socketio.on('ice', '/chat')
def get_message(data):
    uuid = data['uuid']
    room = data['room']
    emit('ice', data, room=room)


socketio.run(app, host='0.0.0.0', port=5000, debug=True, certfile='certs/cert.pem', keyfile='certs/key.pem')
